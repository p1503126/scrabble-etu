#include "PlayersLetters.hpp"


using namespace std;

PlayersLetters::PlayersLetters(){
    //The hand is emptied
    hand.clear();
}

PlayersLetters::~PlayersLetters(){

}

void PlayersLetters::recharge(BagOfLetters &bol){
    cout << "RECHARGE" << endl;
    //And while the player doesn't have 7 letters in his hand
    while(bol.isEmpty() == false && hand.size() < 7){
        hand.push_back(bol.pickLetter(bol.bag));
    }
}

void PlayersLetters::initHand(BagOfLetters &bol){

    //The hand is empty at the beginning so the player have to pick 7 letters
    for(int i = 0; i < 7; i++){
        hand.push_back(bol.pickLetter(bol.bag));
    }
    /*Letter A;
    A.id = 'A';
    A.num = 1;
    A.points = 1;
    Letter B;
    B.id = 'B';
    B.num = 2;
    B.points = 3;
    Letter R;
    R.id = 'R';
    R.num = 18;
    R.points = 1;
    Letter E;
    E.id = 'E';
    E.num = 5;
    E.points = 1;
    Letter J;
    J.id = 'J';
    J.num = 10;
    J.points = 8;
    Letter S;
    S.id = 'S';
    S.num = 19;
    S.points = 1;
    Letter X;
    X.id = 'X';
    X.num = 24;
    X.points = 10;


    hand.push_back(A);
    hand.push_back(B);
    hand.push_back(J);
    hand.push_back(R);
    hand.push_back(J);
    hand.push_back(E);
    hand.push_back(S);*/
}


void PlayersLetters::display() {
    for (int i = 0; i < hand.size(); i++) {
        std::cout << hand[i].id << " ";
    }
}
