#pragma once
#include "Letter.hpp"
#include <vector>

using namespace std;

class BagOfLetters{
    public:
        //Constructor
        BagOfLetters();

        //Destructor
        ~BagOfLetters();

        vector<Letter> bag;

        //Initializes the number of each letter
        void initNumberOfLetter();

        //Picks random Letter
        Letter pickLetter(vector<Letter> &bag);

        //Inserts a Letter
        void insert();

        //Shuffles the letters in bag vector
        void shuffleBag();

        //Sees if the bag is empty
        bool isEmpty();

        //Initializes the points for each letter
        void letterPoints();

        int pointsOfLetter[27];

    private:
        Letter l;

        int numberOfLetter[27];

};
