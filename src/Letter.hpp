#pragma once

class Letter {
    public:
        char id;
        int num;
        int points;

        // Constructor (default)
        Letter();

        // Constructor
        //Letter(char cons_id, int cons_num, int cons_points);
        
        // Change an int into a char (for example : 1 to A)
        void int_to_char();
};