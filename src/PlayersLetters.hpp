#pragma once
#include "BagOfLetters.hpp"

#include <iostream>
#include <vector>

using namespace std;

class PlayersLetters{
    public:
        //Constructor
        PlayersLetters();

        //Destructor
        ~PlayersLetters();

        //Recharges the player's hand
        void recharge(BagOfLetters &bol);

        //Initializes the player's hand
        void initHand(BagOfLetters &bol);

        //Display the player's hand
        void display();


        vector<Letter> hand;
};
