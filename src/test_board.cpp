#include "board.hpp"
//#include "Letter.hpp"
//#include "ArbreDico.hpp"
#include "solver.hpp"
#include <iostream>
#include <sstream>


int main() {


  BagOfLetters bag;
  PlayersLetters player;
  player.initHand(bag);


  Board b ;
  Arbre Tree;

  Tree.addFromFile("data/dico.txt");

  std::cout << b << std::endl ;

  std::stringstream ss ;
  ss << "..............." << std::endl ;
  ss << "..............." << std::endl ;
  ss << "..............." << std::endl ;
  ss << "..............." << std::endl ;
  ss << "..............." << std::endl ;
  ss << "..............." << std::endl ;
  ss << "..............." << std::endl ;
  ss << "....PROJET....." << std::endl ;
  ss << ".......O......." << std::endl ;
  ss << ".......U......." << std::endl ;
  ss << ".......E......." << std::endl ;
  ss << ".......U......." << std::endl ;
  ss << ".....SCRABBLE.." << std::endl ;
  ss << "..............." << std::endl ;
  ss << "..............." << std::endl ;


  b.load(ss) ;

  std::cout << b << std::endl ;

  Solver solv(Tree, player, b);
  b.availableSpots = adjacent(b);
  for (int i = 0; i < b.availableSpots.size(); i++)  std::cout << "autorise : " << b.availableSpots[i] << std::endl;

  player.display();

  solv.Loop(143);
  //solv.LoopSpots();

  player = solv.player;
  player.display();
  player.recharge(bag);
  player.display();

  Tree.deleteArbre(Tree.root);

  return 0 ;
}
