#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "Node.hpp"

using namespace std;


class Arbre {
    public:
        Node * root;
        int niveau;

        Arbre();

        void deleteArbre(Node * root);

        void insert(string s);

        bool search(string s);

        void addGaddag(string s);


        void addFromFile(string path);

};
