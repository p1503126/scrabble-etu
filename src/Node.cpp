#include <iostream>
#include "Node.hpp"


Node::Node() {
    terminal = false;
    lettre = ' ';
    for (int i=0; i<TAILLE_ALPHABET+1; i++) { // TAILLE_ALPHABET+1 pour le + du GADDAG qui correspond � l'indice 0
        child.push_back(nullptr);
        //cout << i << endl;
    }
}


bool Node::isLeaf() {
    for (int i=0; i<TAILLE_ALPHABET+1; i++) {
        if(child[i] != nullptr) {
            return false;
        }
    }
    return true;
}

void Node::setLettre(char c) {
    lettre = c;
}

char Node::getLettre() {
    return lettre;
}

void Node::setTerminal(bool t) {
    terminal = t;
}

bool Node::getTerminal() {
    return terminal;
}
