#include "BagOfLetters.hpp"
#include <iostream>
#include <time.h>

using namespace std;


BagOfLetters::BagOfLetters(){
    initNumberOfLetter();
    letterPoints();
    insert();
    shuffleBag();
}

BagOfLetters::~BagOfLetters(){

}

// Initializes the table separately from the constructor to modify easier and have a constructor easier to read
void BagOfLetters::initNumberOfLetter() {
    numberOfLetter[0] = 0;
    numberOfLetter[1] = 9;
    numberOfLetter[2] = 2;
    numberOfLetter[3] = 2;
    numberOfLetter[4] = 3;
    numberOfLetter[5] = 15;
    numberOfLetter[6] = 2;
    numberOfLetter[7] = 2;
    numberOfLetter[8] = 2;
    numberOfLetter[9] = 8;
    numberOfLetter[10] = 1;
    numberOfLetter[11] = 1;
    numberOfLetter[12] = 5;
    numberOfLetter[13] = 3;
    numberOfLetter[14] = 6;
    numberOfLetter[15] = 6;
    numberOfLetter[16] = 2;
    numberOfLetter[17] = 3;
    numberOfLetter[18] = 6;
    numberOfLetter[19] = 6;
    numberOfLetter[20] = 6;
    numberOfLetter[21] = 6;
    numberOfLetter[22] = 2;
    numberOfLetter[23] = 1;
    numberOfLetter[24] = 1;
    numberOfLetter[25] = 1;
    numberOfLetter[26] = 1;

}

// Picks a letter in constant time
Letter BagOfLetters::pickLetter(vector<Letter> &bag){
    Letter p = bag.back();
    bag.pop_back();
    p.num = p.num; // -1 ?
    return p;
}

// Inserts all the letters in the bag
void BagOfLetters::insert() {
    // For each letter
    for (int i = 1; i <= 26; i++) {
        //Creation/Initialization of letter -> possible to do a fonction at some point
        Letter l;
        l.num = i;
        l.int_to_char();

        // Uses the table that gives the occurence of each letter to push the exact amount of letter in the bag
        for (int j = 1; j <= numberOfLetter[i]; j++) {
            bag.push_back(l);
        }
    }
}

// Shuffles the bag like a card game so that the pick takes the last letter in the vector bag which will be random
void BagOfLetters::shuffleBag() {
    srand( (unsigned)time(NULL) );
    //We create a second bag in which we will put the letters in disorder, which are in order in bag
    vector<Letter> secBag;
       //cout << "non shuffled" << endl;
    /*for (int i = 0; i < bag.size(); i ++) {
        cout << bag[i].id << endl;
    }*/
    while(bag.size() > 0){
        // We take a random letter
        int index = rand()%bag.size();
        // We put it in the second bag
        secBag.push_back(bag[index]);
        // We erase it from bag
        bag.erase(bag.begin()+index);
    }


    bag = secBag;

    /*cout << "shuffled" << endl;
    for (int i = 0; i < bag.size(); i ++) {
        cout << bag[i].id << endl;
    }*/
}

bool BagOfLetters::isEmpty(){
    if(bag.size() == 0){
        return true;
    }
    return false;
}

// Initializes the table separately from the constructor to modify easier and have a constructor easier to read
void BagOfLetters::letterPoints(){
    pointsOfLetter[0] = 0;
    pointsOfLetter[1] = 1;
    pointsOfLetter[2] = 3;
    pointsOfLetter[3] = 3;
    pointsOfLetter[4] = 2;
    pointsOfLetter[5] = 1;
    pointsOfLetter[6] = 4;
    pointsOfLetter[7] = 2;
    pointsOfLetter[8] = 4;
    pointsOfLetter[9] = 1;
    pointsOfLetter[10] = 8;
    pointsOfLetter[11] = 10;
    pointsOfLetter[12] = 1;
    pointsOfLetter[13] = 2;
    pointsOfLetter[14] = 1;
    pointsOfLetter[15] = 1;
    pointsOfLetter[16] = 3;
    pointsOfLetter[17] = 2;
    pointsOfLetter[18] = 1;
    pointsOfLetter[19] = 1;
    pointsOfLetter[20] = 1;
    pointsOfLetter[21] = 1;
    pointsOfLetter[22] = 4;
    pointsOfLetter[23] = 10;
    pointsOfLetter[24] = 10;
    pointsOfLetter[25] = 10;
    pointsOfLetter[26] = 10;

}
