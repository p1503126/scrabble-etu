#include "solver.hpp"

#include <iostream>

using namespace std;

Solver::Solver() {
    scores.clear();
    solutions.clear();
    sens.clear();
    solFinal.clear();
    //arbreVec.clear();
    playableLetters.clear();
    bVec.clear();
    currentNode.clear();
    currentCoord.clear();
    bVecFinal.clear();
    handFinal.clear();
    coordFinal.clear();
    bVecFinalBySpot.clear();
    handFinalBySpot.clear();
    solFinalBySpot.clear();
    scoresBySpot.clear();
}

Solver::Solver(Arbre a, PlayersLetters p, Board board) {
    scores.clear();
    solutions.clear();
    sens.clear();
    //arbreVec.clear();
    playableLetters.clear();
    bVec.clear();
    bVecFinal.clear();
    currentNode.clear();
    currentCoord.clear();
    handFinal.clear();
    coordFinal.clear();
    bVecFinalBySpot.clear();
    handFinalBySpot.clear();
    solFinalBySpot.clear();
    scoresBySpot.clear();
    arbre = a;
    player = p;
    b = board;
}


void Solver::initSolutions(int coordStart) {
    currentCoord.push_back(coordStart);
    string s;
    for (unsigned int i = 0; i < player.hand.size(); i++) { //  charge les lettres du joueur pour le solver
        s = s+player.hand[i].id;
    }
    playableLetters.push_back(s);
    solutions.push_back("");

    currentNode.push_back(arbre.root);

    bVec.push_back(b);

    sens.push_back(-1); // reculons
}

void Solver::nextStep(int ref, int coordStart) {
    int index = currentNode.size() - ref -1;
    Node * temp = new Node();
    temp = currentNode[ref];
    bool avoidDuplicateGaddag = false;

    /*for (int i = 0; i < solFinal.size(); i++) {
        cout << "solutionF so far : " << solFinal[i] << endl;
    }*/

    /*for (int i = 0; i < solutions.size(); i++) {
        cout << "solution so far : " << solutions[i] << endl;
        cout << "currentCoord : " << currentCoord[i] << endl;
        cout << "currentNode letter : " << currentNode[i]->lettre << endl;
        cout << "currentNode terminal : " << currentNode[i]->terminal << endl << endl;
    }*/

    if (temp->terminal) {
        bool duplicateSolution = false;
        for (unsigned int l = 0; l < solFinal.size(); l++) {
            if (solutions[ref] == solFinal[l]) {
                duplicateSolution = true;
            }
        }
        if (!duplicateSolution) {
            solFinal.push_back(solutions[ref]);
            bVecFinal.push_back(bVec[ref]);
            handFinal.push_back(playableLetters[ref]);
            cout << "ajout solution : " << solutions[ref] << endl;
        }
    }

    if(b.spots[currentCoord[ref]].letter == 0) { // si la case actuelle est bien vide, on fait les etats pour chaque lettre jouable
        //cout << "case vide" << endl;

        for (unsigned int i = 0; i < playableLetters[ref].size(); i++) {
            // ETAT N LETTRES
            int num = (int)playableLetters[ref][i] - 64;
            //if (temp == nullptr) cout << "checks out" << endl;
            /*if (temp->terminal) {
                solFinal.push_back(solutions[ref]);
                bVecFinal.push_back(bVec[ref]);
            }*/
            if (temp->child[num] != nullptr) { // si lettre existe apres dans gaddag

                index++;

                // cree nouvel etat correspondant
                currentNode.push_back(temp->child[num]); // avance au noeud suivant pour le nouvel etat
                bVec.push_back(bVec[ref]);
                playableLetters.push_back(playableLetters[ref]);
                currentCoord.push_back(currentCoord[ref]);
                solutions.push_back(solutions[ref]);
                sens.push_back(sens[ref]); // on garde le sens tant qu'on a pas pass� +
                bVec[ref+index].spots[currentCoord[ref+index]].letter = playableLetters[ref+index][i]; // on rajoute la lettre au plateau
                solutions[ref+index] = solutions[ref+index]+playableLetters[ref+index][i];

                //cout << bVec[ref+index] << endl;
                playableLetters[ref+index].erase(playableLetters[ref+index].begin()+i);
                cout << "playable letters : " << playableLetters[ref+index] << " / " << solutions[ref+index] << endl;

                currentCoord[ref+index] = currentCoord[ref+index] +(sens[ref+index]); // on recule

                /*if (temp->child[num]->terminal) {
                        solFinal.push_back(solutions[ref]);
                        bVecFinal.push_back(bVec[ref]);
                        cout << "ajout solution num : " << solutions[ref] << endl;
                }*/
            }
            //et pour le gaddag
            if (temp->child[0] != nullptr) { // mais qu'on a le +
                if (!avoidDuplicateGaddag) {
                    //temp = temp->child[0]; // on voit ce qui a apr�s, on se repositionne, etc
                    if (b.spots[currentCoord[ref]+sens[ref]].letter != 0) { // si le spot d'apres est pris par une lettre
                            cout << "STOP spot apres pris par lettre" << endl;
                            //return;
                    }
                    else {
                        // nouvel etat en changeant de sens (vers la droite)
                        index++;
                        currentNode.push_back(temp->child[0]); // avance au noeud suivant pour le nouvel etat
                        bVec.push_back(bVec[ref]);
                        playableLetters.push_back(playableLetters[ref]);
                        solutions.push_back(solutions[ref]);
                        solutions[ref+index] = solutions[ref+index] + '+';
                        currentCoord.push_back(coordStart); // on se replace au debut
                        sens.push_back(1); // on avance
                        //cout << bVec[ref+index] << endl;
                        currentCoord[ref+index] = currentCoord[ref+index] + (sens[ref+index]); // on avance
                    }
                   /* if (temp->child[0]->terminal) {
                            solFinal.push_back(solutions[ref]);
                            bVecFinal.push_back(bVec[ref]);
                            cout << "ajout solution 0 : " << solutions[ref] << endl;
                    }*/
                    avoidDuplicateGaddag = true;
                }

            } // fin if (temp->child[0] != nullptr)

        } // FIN FOR

    }

    else { // si la case n'est pas vide, on doit regarder quelle lettre est presente et si ca colle dans le gaddag
        //cout << "lettre presente : " << b.spots[currentCoord[ref]].letter << endl;
        int n = (int)b.spots[currentCoord[ref]].letter - 64;

        if (temp->child[n] != nullptr) { // la lettre presente fonctionne
            // cree nouvel etat correspondant
            index++;
            currentNode.push_back(temp->child[n]); // avance au noeud suivant pour le nouvel etat
            bVec.push_back(bVec[ref]);
            //cout << bVec[ref+index] << endl;
            playableLetters.push_back(playableLetters[ref]);
            currentCoord.push_back(currentCoord[ref]);
            solutions.push_back(solutions[ref]);
            solutions[ref+index] = solutions[ref+index] + b.spots[currentCoord[ref]].letter;
            sens.push_back(sens[ref]);
            currentCoord[ref+index] = currentCoord[ref+index]+sens[ref+index]; // on recule
            /*if (temp->child[n]->terminal) {
                solFinal.push_back(solutions[ref]);
                bVecFinal.push_back(bVec[ref]);
                cout << "ajout solution n : " << solutions[ref] << endl;
            }*/
        }

        else { // la lettre presente ne colle pas dans gaddag
            cout << "lettre presente " <<  b.spots[currentCoord[ref]].letter << " incompatible" << endl;
        }
    }


    //cout << "erasing" << endl;
    // on vire l'etat de depart qui a genere ces nouveaux etats
    playableLetters.erase(playableLetters.begin()+ref);
    bVec.erase(bVec.begin()+ref);
    currentNode.erase(currentNode.begin()+ref);
    currentCoord.erase(currentCoord.begin()+ref);
    solutions.erase(solutions.begin()+ref);
    sens.erase(sens.begin()+ref);
}

int Solver::calcScore(Board b, int coordStart) {
    BagOfLetters bol;
    int points = 0;
    int wordbonus = 1;
    while (b.spots[coordStart].letter != 0) {
        coordStart--;
        //cout << "coord : " << coordStart << endl;
    }
    coordStart++;
    //cout << "coord depart compte points : " << coordStart << endl;

    while(b.spots[coordStart].letter != 0) {
        int num = (int)b.spots[coordStart].letter - 64;
        points = points + bol.pointsOfLetter[num]*b.spots[coordStart].bonus.letter_factor;
        //cout << "points " << points << endl;
        wordbonus = wordbonus * b.spots[coordStart].bonus.word_factor;
        //cout << "word bonus : " << wordbonus << endl;
        coordStart++;
    }
    points = points * wordbonus;
    //cout << "POINTS : " << points << endl;
    return points;
}


void Solver::calcScoreBoards(int coordStart) {
    BagOfLetters bol;
    for (unsigned int i = 0; i < bVecFinal.size(); i++) {
        scores.push_back(calcScore(bVecFinal[i], coordStart));
    }
    cout << "TOUS LES SCORES ONT ETE CALCULES" << endl;
}





void Solver::Loop(int coordStart) {
    initSolutions(coordStart);
    int numEtatRef = 1;
     while(true) {
        //while (numEtatRef > 0) {
            numEtatRef = bVec.size() -1;
            if (numEtatRef == -1) {
                    cout << "BREAK -1" << endl;
                    break;
                    //return;
            }
            cout << endl << "num : " << numEtatRef << endl;
            //string breaker = "";
            //cin >> breaker;
            nextStep(numEtatRef, coordStart);

        //}
    }

    calcScoreBoards(coordStart);

    for (unsigned int i = 0; i < solFinal.size(); i++) {
        cout << "SOLUTION " << i << " : " << solFinal[i] << endl;
        cout << "SCORE : " << scores[i] << endl;
        cout << "LETTRES RESTANTES : " << handFinal[i] << endl;
        //cout << bVecFinal[i] << endl;
    }
    cout << endl;

    int indexBest = max_element(scores.begin(), scores.end()) - scores.begin();

    cout << "%% BEST FOR SPOT %%" << endl;
    cout << solFinal[indexBest] << " / " << scores[indexBest] << endl;
    cout << "LETTRES RESTANTES : " << handFinal[indexBest] << endl;
    cout << bVecFinal[indexBest] << endl;

    b = bVecFinal[indexBest];
    unsigned int j = 0;
    for (unsigned int i = 0; i <= handFinal[indexBest].size(); i++) {
        //cout << "i : " << i << " j : " << j << endl;
        while (player.hand[j].id != handFinal[indexBest][i] && j < player.hand.size()) {
            player.hand.erase(player.hand.begin()+i);
        }

        if (player.hand[j].id == handFinal[indexBest][i]) {
            j++;
        }
    }

    /*solFinalBySpot.push_back(solFinal[indexBest]);
    bVecFinalBySpot.push_back(bVecFinal[indexBest]);
    scoresBySpot.push_back(scores[indexBest]);
    handFinalBySpot.push_back(handFinal[indexBest]);
    coordFinal.push_back(coordStart);*/

}


/*void Solver::LoopSpots() {
    b.availableSpots.push_back(143);
    b.availableSpots.push_back(158);
    cout << "test : " << b.availableSpots.size() << endl;
    for (int i = 0; i < b.availableSpots.size(); i++) {
        cout << b.availableSpots[i] << endl;
        Loop(b.availableSpots[i]);
    }
    int indexBest = max_element(scoresBySpot.begin(), scoresBySpot.end()) - scoresBySpot.begin();
    cout << "##### BEST ####" << endl;
    cout << "COORD DEPART : " << coordFinal[indexBest] << endl;
    cout << solFinalBySpot[indexBest] << " / " << scoresBySpot[indexBest] << endl;
    cout << "LETTRES RESTANTES : " << handFinalBySpot[indexBest] << endl;
    cout << bVecFinalBySpot[indexBest] << endl;

    // actualise le board et met � jour la main du joueur (enleve les lettres utilisees)
    b = bVecFinalBySpot[indexBest];
    int j = 0;
    for (int i = 0; i <= handFinalBySpot[indexBest].size(); i++) {
        //cout << "i : " << i << " j : " << j << endl;
        while (player.hand[j].id != handFinalBySpot[indexBest][i] && j < player.hand.size()) {
            player.hand.erase(player.hand.begin()+i);
        }

        if (player.hand[j].id == handFinalBySpot[indexBest][i]) {
            j++;
        }
    }
}*/

