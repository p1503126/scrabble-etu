#include <iostream>
#include "ArbreDico.hpp"


Arbre::Arbre() {
    root = new Node();
    niveau = 0;

}


void Arbre::deleteArbre(Node * root) {
    if (root == nullptr) {
        return;
    }

    for (int i=0; i<TAILLE_ALPHABET+1; i++) {
        deleteArbre(root->child[i]);
    }

    delete root;
}


void Arbre::insert(string s) {
    Node * temp = root;
    int index = -1;

    for (unsigned int i=0; i<s.size(); i++) {

        //cout << "index : " << index << endl;

        if (i == 1) { // avant de rajouter la 2e lettre (indice 1)
                index = 0; // symbole du gaddag stock� dans child[0]
                if (temp->child[index] == nullptr) {
                    temp->child[index] = new Node();
                    temp->child[index]->lettre = '+'; // GADDAG symbol
                    //cout << "inserted : " << '+' << endl;
                }
                temp = temp->child[index];
        }

         index = (int)s[i]-64;
        //cout << "index " << index << " s[i] " << s[i] << " i " << i << endl;
        if (temp->child[index] == nullptr) {
            temp->child[index] = new Node();
            temp->child[index]->lettre = s[i];
            //cout << "inserted : " << s[i] << endl;
        }

        if (i != s.size()-1) {
            //cout << "temp : " << temp->lettre << " child : " << temp->child[index]->lettre << endl;
            temp = temp->child[index];
        }

        else {
            temp->child[index]->terminal = true;
            //cout << "final letter INSERT : " << temp->child[index]->lettre << " for " << s << endl << endl;
        }

        int niv = i +1;
        if (niv > niveau) {
            niveau = niv;
            //cout << "niveau : " << niveau << endl;
        }

    }


}

bool Arbre::search(string s) { // fonctionne avec le GADDAG "de base"pour l'instant
    Node * temp = root;
    int index;

    if (s.find('+') == string::npos) { // + not found in s, add + after first letter
        s.insert(1,1,'+');
        //cout << "check : " << s << endl;
    }


    for (unsigned int i=0; i <= s.size(); i++) {

        if (s[i] == '+') {
            index = 0;
        }

        else {
            index = (int)s[i]-64;
        }

        if (temp->child[index] == nullptr) { // lettre non presente
            /*if (temp->child[0] == nullptr) { // pas de symbole gaddag +
                cout << "false pas de symbole gaddag" << endl;
                return false;
            }
            else {
                temp = temp->child[0];
                if (temp->child[index] == nullptr) {*/
                    cout << "false at " << s[i] << endl;
                    return false;
                //}
            //}
        }
        //cout << temp->child[index]->lettre << " ";
        temp = temp->child[index];
    }

    //cout << "true"  << endl;
    return true;
}


void Arbre::addGaddag(string s) {

    int var = s.size(); // nombre de variations du mot = nombre de lettres, sachant qu'on a d�j� le mot de base dans l'ordre e. g. B+ATEAU
    if (var == 2) return;
    int index = -1;
    //cout << "--- GADDAG start ---" << endl;
    for (unsigned int j=1; j < var; j++) { // pas 0 car on a la premiere iteration via insert
            string check;
            Node * temp = root;
            // words have at least 2 letters
            //cout << "*****" << endl;
            /// reversed sub string
            for (int k = j; k >= 0; k--) { // decreasing loop because why not
                index = (int)s[k]-64;

                if (temp->child[index] == nullptr) {
                    temp->child[index] = new Node();
                    temp->child[index]->lettre = s[k];
                    //cout << "inserted : " << s[k] << endl;

                }
                else {
                    //cout << "already there : " << s[k] << endl;
                }
                //cout << temp->child[index]->lettre;

                check.push_back(temp->child[index]->lettre);
                temp = temp->child[index];
            }

            /// gaddag symbol
            index = 0;

            if (temp->child[index] == nullptr) {
                temp->child[index] = new Node();
                temp->child[index]->lettre = '+'; // GADDAG symbol
                //cout << "inserted : " << '+' << endl;
            }
            else {
                //cout << "already there : " << '+' << endl;
            }
            //cout << temp->child[index]->lettre;
            check.push_back(temp->child[index]->lettre);
            if (j == var -1) {
                temp->child[index]->terminal = true; // last gaddag variation is word backward with gaddag sign at the end (terminal)
                //cout << endl;
            }
            temp = temp->child[index];

            /// rest of the string in regular order
            for (unsigned int i=j+1; i<s.size(); i++) {
                index = (int)s[i]-64;

                if (temp->child[index] == nullptr) {
                    temp->child[index] = new Node();
                    temp->child[index]->lettre = s[i];
                    //cout << "inserted : " << s[i] << endl;
                }
                else {
                    //cout << "already there : " << s[i] << "/";
                    //cout <<  temp->child[index]->lettre << endl;
                }

                //cout << temp->child[index]->lettre;
                check.push_back(temp->child[index]->lettre);

                if (i != s.size()-1) {
                    temp = temp->child[index];
                }
                else {
                    //cout << endl << "final letter : " << temp->child[index]->lettre << " for " << s << endl;
                    //cout << endl;
                    temp->child[index]->terminal = true;

                }

                int niv = i+1;
                if (niv > niveau) {
                    niveau = niv;
                }

            }
    }

     //cout << "--- GADDAG for "  << s << "  ended ---" << endl;

}


void Arbre::addFromFile(string path) {
    fstream dico;
    string word;
    dico.open(path);

    //int i = 0;
    while((getline (dico, word))) {
        //cout << word << endl;
        insert(word);
        addGaddag(word);
        //i++;
    }

  dico.close();
}

