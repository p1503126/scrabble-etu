#include <iostream>
#include <vector>

using namespace std;

const int TAILLE_ALPHABET = 26;


class Node {
    public:
        Node();

        bool terminal;
        char lettre;
        vector <Node *> child;

        bool isLeaf();


        void setLettre(char c);
        char getLettre();

        void setTerminal(bool t);
        bool getTerminal();
};
