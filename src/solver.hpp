#include "ArbreDico.hpp"
#include "board.hpp"
#include "PlayersLetters.hpp"

#include <iostream>

class Solver {
    public:
        PlayersLetters player;
        Arbre arbre;
        Board b;
        vector <int> scores;
        vector <int> scoresBySpot;
        vector <string> solutions;
        vector <string> solFinal;
        vector <string> solFinalBySpot;
        //vector <Arbre> arbreVec;
        vector <string> playableLetters;
        vector <Board> bVec;
        vector <Board> bVecFinal;
        vector <Board> bVecFinalBySpot;
        vector <Node*> currentNode;
        vector <int> currentCoord;
        vector <int> sens;
        vector <string> handFinal;
        vector <string> handFinalBySpot;
        vector <int> coordFinal;
        Solver();
        Solver(Arbre a, PlayersLetters p, Board board);


        void nextStep(int ref, int coordStart);
        void initSolutions(int coordStart);
        void Loop(int coordStart);
        int calcScore (Board b, int coordStart);
        void calcScoreBoards(int coordStart);
        void LoopSpots();
};
